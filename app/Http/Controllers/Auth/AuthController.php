<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Exception;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        // return $request->all();
        try {
            $request->validate([
                'email' => 'required|unique:users',
                'password' => 'required|min:6|max:10'
            ]);

            $id = $request->input('id');

            if ($id > 0) {
                $user = User::find($id);
            } else {
                $user = new User();
            }

            $user->full_name    = $request->input('full_name');
            $user->phone        = $request->input('phone');
            $user->role_id      = $request->input('role_id') ? $request->input('role_id') : 0;
            $user->user_name    = $request->input('user_name');
            $user->phone        = $request->input('phone');
            $user->email        = $request->input('email');
            $user->password     = Hash::make($request->input('password'));

            // reutn $user;
            $user->save();

            // return $user;
            if ($user->save) {
                Auth::login($user);
                Alert::success('Congrats', 'You\'ve Successfully Registered');
                return redirect('/admin');
            }

            return redirect('/admin');
        } catch (Exception $e) {
            Alert::error('Failed!', 'registration failed!');
            return  $e->getMessage();
        }
    }

    public function login(Request $request)
    {
        try {
            if (Auth::attempt($request->only('email', 'password'))) {
                return redirect('/admin');
            }

            return back()->with('error', 'Your Credentials is incorrect');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }
}
