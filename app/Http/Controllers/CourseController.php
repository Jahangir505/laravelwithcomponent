<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Exception;
use Helper;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::orderBy('id', 'desc')->latest()->paginate(10);
        // return $courses;
        return view('homeWork.course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('homeWork.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required',
                'start_date' => 'required',
                'instructor_name' => 'required',
            ]);
            $course = new Course();
            $course->title = $request->input('title');
            $course->batch_no = $request->input('batch_no');
            $course->start_date = $request->input('start_date');
            $course->end_date = $request->input('end_date');
            $course->instructor_name = $request->input('instructor_name');
            $course->banner_uri = Helper::uploadImage($request->file('image'), 'courses');
            $course->is_active = $request->input('is_active') ? true : false;
            $course->course_type = $request->input('course_type');
            $course->save();
            Alert::success('Great Job!', 'Course Create Successfully!');
            return redirect()->route('courses.index');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $course = Course::find($id);
            return view('homeWork.course.show', compact('course'));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $course = Course::find($id);

            return view('homeWork.course.edit', compact('course'));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $course = Course::find($id);
            $course->title = $request->input('title');
            $course->batch_no = $request->input('batch_no');
            $course->start_date = $request->input('start_date');
            $course->end_date = $request->input('end_date');
            $course->instructor_name = $request->input('instructor_name');
            $course->banner_uri = Helper::uploadImage($request->file('image'), 'courses') ?? $course->banner_uri;
            $course->is_active = $request->input('is_active') ? true : false;
            $course->course_type = $request->input('course_type');
            $course->save();
            Alert::success('Great Job!', 'Course Update Successfully!');
            return redirect()->route('courses.index');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $course = Course::find($id);
            $course->delete();
            Alert::success('Great Job!', 'Course Delete Successfully!');
            return redirect()->route('courses.index');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
