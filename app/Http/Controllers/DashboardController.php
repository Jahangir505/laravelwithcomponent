<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        return view('backend.dashboard');
    }



    public function create()
    {
        return view('backend.products.create');
    }

    public function login()
    {
        if (Auth::check()) {
            return redirect('/admin');
        }
        return view('backend.login');
    }

    public function registration()
    {
        return view('backend.register');
    }
}
