<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        return view('frontend.home');
    }

    public function productList()
    {
        return view('frontend.category_wise_product_list');
    }

    public function productDetails()
    {
        return view('frontend.details');
    }

    public function productCart()
    {
        return view('frontend.cart');
    }

    public function checkout()
    {
        return view('frontend.checkout');
    }

    public function order()
    {
        return view('frontend.order');
    }
}
