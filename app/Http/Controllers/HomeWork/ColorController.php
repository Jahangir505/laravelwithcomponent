<?php

namespace App\Http\Controllers\HomeWork;

use App\Http\Controllers\Controller;
use App\Models\Color;
use Exception;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $colors = Color::orderBy('id', 'desc')->paginate(10);
            return view('homeWork.color.index', compact('colors'));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('homeWork.color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'name' => 'required|unique:users'
        // ]);
        try {
            Color::create([
                'title' => $request->input('title'),
                'color_code' => $request->input('color')
            ]);
            Alert::success('Great Job!', 'Color Create Successfully!');
            return redirect()->route('colors.index');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $color = Color::find($id);
        return view('homeWork.color.edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $color = Color::find($id);
            $color->update([
                'title' => $request->input('title'),
                'color_code' => $request->input('color'),
            ]);
            Alert::success('Great Job!', 'Color Update Successfully!');
            return redirect()->route('colors.index');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $color = Color::find($id);
            $color->delete();
            Alert::info('Great Job!', 'Color Delete Successfully!');
            return redirect()->route('colors.index');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
