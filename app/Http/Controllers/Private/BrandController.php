<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::where('is_deleted', '=', 0)->orderBy('id', 'desc')->get();
        return view('backend.brand.index', compact('brands'));
    }

    public function create()
    {
        return view('backend.brand.create');
    }

    public function store(Request $request)
    {
        // return $request->all();
        $id = $request->input('id');

        if ($id > 0) {
            $brand = Brand::find($id);
        } else {
            $brand = new Brand();
        }

        $brand->name = $request->input('name');
        $brand->save();


        if ($brand->save()) {
            if ($id > 0) {
                Alert::success('Success Title', 'Brand Update Successfully!');
            }
            Alert::success('Success Title', 'Brand Create Successfully!');
            return redirect('/admin/brands');
        }

        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');

        $brand = Brand::where('id', '=', $id)->first();

        return view('backend.brand.edit', compact('brand'));
    }

    public function delete(Request $request)
    {
        $id = $request->input('id');

        $brand = Brand::where('id', '=', $id);

        if (!empty($brand)) {
            $brand = $brand->first();
            $brand->is_deleted = 1;
            $brand->save();
            return redirect('/admin/brands');
        }
        session()->flash("success", "This is success message");
        return redirect()->back();
    }
}
