<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Helper;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CategoryController extends Controller

{



    public function index()
    {
        if (!Gate::allows('admin')) {
            return redirect()->route('error.403');
        }
        $categories = Category::where('is_deleted', '=', 0)->orderBy('id', 'desc')->get();
        return view('backend.category.index', compact('categories'));
    }

    public function create()
    {
        return view('backend.category.create');
    }

    public function store(Request $request)
    {
        // return $request->all();
        $id = $request->input('id');

        $request->validate([
            'name' => 'required',
            'description' => 'required|min:10|max:100'
        ]);
        $file = $request->file('image');
        if ($id > 0) {
            $category = Category::find($id);
            if ($request->file('image')) {
                unlink(storage_path("app/public/categories/") . $file->image);
            }
        } else {
            $category = new Category();
        }

        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $category->image_uri = Helper::uploadImage($request->file('image'), 'categories');
        $category->save();

        if ($category->save()) {
            if ($id > 0) {
                Alert::success('Great Job!', 'Category Update Successfully!');
            }
            Alert::success('Great Job!', 'Category Create Successfully!');

            return redirect('/admin/categories');
        }

        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');

        $category = Category::where('id', '=', $id)->first();

        return view('backend.category.edit', compact('category'));
    }

    public function delete(Request $request)
    {
        $id = $request->input('id');

        $category = Category::where('id', '=', $id);

        if (!empty($category)) {
            $category = $category->first();
            $category->is_deleted = 1;
            $category->save();

            return redirect('/admin/categories');
        }

        return redirect()->back();
    }
}
