<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Unit;
use Illuminate\Support\Str;
use Exception;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::where('is_deleted', '=', 0)->with('category')->with('brand')->with('units')->get();
        // return var_dump($products);
        return view('backend.products.product', compact('products'));
    }

    public function create()
    {
        $brands = Brand::where('is_deleted', '=', 0)->get();
        $categories = Category::where('is_deleted', '=', 0)->get();
        $units = Unit::where('is_deleted', '=', 0)->get();
        return view('backend.products.create', compact('brands', 'categories', 'units'));
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required',
            ]);
            $product = new Product();
            $product->code = time();
            $product->slug = Str::of($request->input('name'))->slug('-');
            $product->category_id = $request->input('category_id');
            $product->brand_id = $request->input('brand_id');
            $product->unit_id = $request->input('unit_id');
            $product->title = $request->input('name');
            $product->description = $request->input('description');
            $product->sale_price = $request->input('sale_price');
            $product->purchase_price = $request->input('purchase_price');
            $product->min_stock = $request->input('min_stock');
            $product->save();
            Alert::success('Great Job!', 'Product Create Successfully!');
            return redirect('/admin/products');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
