<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::where('is_deleted', '=', 0)->orderBy('id', 'desc')->get();
        return view('backend.supplier.index', compact('suppliers'));
    }

    public function create()
    {
        return view('backend.supplier.create');
    }

    public function store(Request $request)
    {
        // return $request->all();
        $id = $request->input('id');

        if ($id > 0) {
            $supplier = Supplier::find($id);
        } else {
            $supplier = new Supplier();
        }

        $supplier->name = $request->input('name');
        $supplier->email = $request->input('email');
        $supplier->phone = $request->input('phone');
        $supplier->address = $request->input('address');
        $supplier->save();

        if ($supplier->save()) {
            return redirect('/admin/suppliers');
        }

        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');

        $supplier = Supplier::where('id', '=', $id)->first();

        return view('backend.supplier.edit', compact('supplier'));
    }

    public function delete(Request $request)
    {
        $id = $request->input('id');

        $supplier = Supplier::where('id', '=', $id);

        if (!empty($supplier)) {
            $supplier = $supplier->first();
            $supplier->is_deleted = 1;
            $supplier->save();

            return redirect('/admin/suppliers');
        }

        return redirect()->back();
    }
}
