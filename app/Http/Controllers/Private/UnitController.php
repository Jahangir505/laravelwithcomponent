<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use App\Models\Unit;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class UnitController extends Controller
{
    public function index()
    {
        $units = Unit::where('is_deleted', '=', 0)->orderBy('id', 'desc')->get();
        return view('backend.unit.index', compact('units'));
    }

    public function create()
    {
        return view('backend.unit.create');
    }

    public function store(Request $request)
    {
        // return $request->all();
        $id = $request->input('id');

        $request->validate([
            'name' => 'required',
        ]);

        if ($id > 0) {
            $unit = Unit::find($id);
        } else {
            $unit = new Unit();
        }

        $unit->name = $request->input('name');
        $unit->save();


        if ($unit->save()) {
            if ($id > 0) {
                Alert::success('Great!', 'Unit Update Successfully!');
            }
            Alert::success('Great!', 'Unit Create Successfully!');
            return redirect('/admin/units');
        }

        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');

        $unit = Unit::where('id', '=', $id)->first();

        return view('backend.unit.edit', compact('unit'));
    }

    public function delete(Request $request)
    {
        $id = $request->input('id');

        $unit = Unit::where('id', '=', $id);

        if (!empty($unit)) {
            $unit = $unit->first();
            $unit->is_deleted = 1;
            $unit->save();
            return redirect('/admin/units');
        }
        session()->flash("success", "This is success message");
        return redirect()->back();
    }
}
