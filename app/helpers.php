<?php

use Image as Image;

class Helper
{

    public static function uploadImage($file = null, $folder = null)
    {
        if (is_null($file)) return $file;



        $fileName = date('dmY') . time() . '.' . $file->getClientOriginalExtension();
        Image::make($file)
            ->resize(300, 200)
            ->save(storage_path("app/public/$folder/" . $fileName));
        return $fileName;
    }
}
