<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'code' => $this->faker->unique()->numberBetween(0, 100),
            'slug' => $this->faker->unique()->sentence(3),
            'category_id' => Category::all()->random()->id,
            'brand_id' => Brand::all()->random()->id,
            'unit_id' => 1,
            'unit_value' => 'KG',
            'title' => $this->faker->unique()->sentence(3),
            'description' => $this->faker->text(40),
            'min_stock' => $this->faker->numberBetween(0, 100),
            'sale_price' => $this->faker->numberBetween(120, 300),
            'purchase_price' => $this->faker->numberBetween(10, 100),
        ];
    }
}
