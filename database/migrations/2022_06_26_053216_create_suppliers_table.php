<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('code', 10)->unique();
            $table->string('full_name', 255)->nullable();
            $table->text('aria_code')->nullable();
            $table->mediumText('address')->nullable();
            $table->mediumText('profile_image_uri')->nullable();
            $table->bigInteger('total_orders')->default(0);
            $table->double('total_order_amount', 18, 2)->default(0);
            $table->double('due', 18, 2)->default(0);
            $table->string('phone_number')->unique()->nullable();
            $table->string('email', 255)->unique()->nullable();
            $table->string('password')->nullable();
            $table->enum('status', ['Active', 'Inactive', 'Banned', 'Pending'])->default('Active');
            $table->string('ip')->nullable();
            $table->string('agent')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->bigInteger('created_by_id')->nullable();
            $table->bigInteger('updated_by_id')->nullable();
            $table->bigInteger('deleted_by_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
};
