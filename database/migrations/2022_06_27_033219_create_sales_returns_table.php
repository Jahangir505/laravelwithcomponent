<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_returns', function (Blueprint $table) {
            $table->id();
            $table->string('code', 10)->unique();
            // $table->date('document_date')->default(DB::raw("CURRENT_DATE()"));
            $table->date('document_date')->nullable();
            $table->bigInteger('customer_id');
            $table->double('total_amount', 18, 2)->default(0);
            $table->double('discount', 18, 2)->default(0);
            $table->double('vat', 18, 2)->default(0);
            $table->double('tax', 18, 2)->default(0);
            $table->double('payment_amount', 18, 2)->default(0);
            $table->double('received_amount', 18, 2)->default(0);
            $table->double('return_amount', 18, 2)->default(0);
            $table->double('paid_amount', 18, 2)->default(0);
            $table->double('due_amount', 18, 2)->default(0);
            $table->enum('payment_type', ['cash', 'credit'])->default('cash');
            $table->enum('status', ['Placed', 'Confirmed', 'Processing', 'Shipping', 'Delivered', 'Canceled', 'Rejected'])->nullable();
            $table->boolean('is_online_order')->default(0);
            $table->string('ip')->nullable();
            $table->string('agent')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->bigInteger('created_by_id')->nullable();
            $table->bigInteger('updated_by_id')->nullable();
            $table->bigInteger('deleted_by_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_returns');
    }
};
