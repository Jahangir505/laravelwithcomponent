<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('code', 10)->unique();
            $table->string('full_name', 255)->nullable();
            $table->text('area_code')->nullable();
            $table->mediumText('profile_image_uri')->nullable();
            $table->double('total_orders', 18, 2)->default(0);
            $table->double('total_order_amount', 18, 2)->default(0);
            $table->double('due', 18, 2)->default(0);
            $table->double('points', 18, 2)->default(0);
            $table->string('personal_phone')->unique()->nullable();
            $table->string('email', 255)->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('nid_number')->nullable();
            $table->mediumText('present_address')->nullable();
            $table->enum('status', ['Active', 'Inactive', 'Banned', 'Pending'])->default('Active');
            $table->string('ip')->nullable();
            $table->string('agent')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->bigInteger('created_by_id')->nullable();
            $table->bigInteger('updated_by_id')->nullable();
            $table->bigInteger('deleted_by_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
