<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code', 10)->unique();
            $table->string('slug', 255)->unique();
            $table->string('barcode', 255)->nullable()->unique();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('unit_id')->nullable();
            $table->string('unit_value', 255)->nullable();
            $table->string('title', 255);
            $table->longText('description')->nullable();
            $table->longText('return_policy')->nullable();
            $table->integer('total_rating_count')->nullable();
            $table->integer('total_review_count')->nullable();
            $table->double('total_stock', 18, 2)->default(0);
            $table->double('min_stock', 18, 2)->default(0);
            $table->double('sale_price', 18, 2)->default(0);
            $table->double('purchase_price', 18, 2)->default(0);
            $table->mediumText('featured_image_uri')->nullable();
            $table->string('featured_video_id', 255)->nullable();
            $table->enum('status', ['Active', 'Inactive', 'Deleted'])->nullable();
            $table->string('ip')->nullable();
            $table->string('agent')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->bigInteger('created_by_id')->nullable();
            $table->bigInteger('updated_by_id')->nullable();
            $table->bigInteger('deleted_by_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
