<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Product::factory(100)->create();

        // User::create([
        //     'full_name' => 'Jahangir Hossain',
        //     'role_id' => '0',
        //     'user_name' => 'jahangir505',
        //     'phone' => '01778175444',
        //     'email' => 'jahangir@gmail.com',
        //     'password' => Hash::make('123456')
        // ]);

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
