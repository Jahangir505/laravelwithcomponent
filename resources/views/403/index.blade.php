<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>403 | Forbidden</title>
    <link rel="stylesheet" href="{{ asset('errors')}}/style.css">
</head>
<body>
    <div class="maincontainer">
        <div class="bat">
          <img class="wing leftwing" 
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-wing.png">
          <img class="body"
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-body.png" alt="bat">
          <img class="wing rightwing"
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-wing.png">
        </div>
        <div class="bat">
          <img class="wing leftwing" 
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-wing.png">
          <img class="body"
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-body.png" alt="bat">
          <img class="wing rightwing"
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-wing.png">
        </div>
        <div class="bat">
          <img class="wing leftwing" 
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-wing.png">
          <img class="body"
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-body.png" alt="bat">
          <img class="wing rightwing"
               src="https://www.blissfullemon.com/wp-content/uploads/2018/09/bat-wing.png">
        </div>
        <img class="foregroundimg" src="https://www.blissfullemon.com/wp-content/uploads/2018/09/HauntedHouseForeground.png" alt="haunted house">
        
      </div>
      <h1 class="errorcode">ERROR 403</h1>
      <div class="errortext">This area is forbidden. Turn back now! <br/> <br/><a href="/" style="background: white;
        padding: 10px 30px;
        border-radius: 10px;
        font-weight: 600; font-size: 14px; text-decoration: none;">Back to Home</a></div>
      
      
</body>
</html>