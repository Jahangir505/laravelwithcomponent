<x-admin.layout.master>
    <x-slot:title>Brand </x-slot:title>
    <x-slot:pageTitle>Create Brand </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
           
            <a href="/admin/brands" class="btn btn-info">Brand List</a>
        </div>
        <form action="/admin/brand/store" method="POST" >
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="name" class="form-label">Name:</label>
                            <input type="text" class="form-control" id="name"  placeholder="Enter Name" name="name">
                          </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="image" class="form-label">Image:</label>
                            <input type="file" class="form-control" id="image"  placeholder="Enter Image" name="image">
                          </div>
                    </div>
                    
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>