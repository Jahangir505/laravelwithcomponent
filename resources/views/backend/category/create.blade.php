<x-admin.layout.master>
    <x-slot:title>Category </x-slot:title>
    <x-slot:pageTitle>Create Category </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
           
            <a href="/admin/categories" class="btn btn-info">Category List</a>
        </div>
        <form action="/admin/category/store" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="name" placeholder="Enter your category name" label="Name:" labelFor="name"  id="name" type="text" value="{{ old('name') }}" /> 
                        <x-admin.utilities.form.error name="name" />
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="image" placeholder="Upload your image" label="Image:" labelFor="name"  id="name" type="file" value="{{ old('name') }}" /> 
                        {{-- <x-admin.utilities.form.error name="name" /> --}}
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="color" placeholder="Upload your image" label="Pick Color:" labelFor="color"  id="color" type="color" class="form-control-color" value="{{ old('color') }}" /> 
                        {{-- <x-admin.utilities.form.error name="name" /> --}}
                    </div>

                    <div class="col-md-12">
                        <x-admin.utilities.form.textarea name="description" placeholder="Enter your category name" label="Description:" labelFor="description"  id="description" type="text" value="{{ old('description') }}" /> 
                        <x-admin.utilities.form.error name="description" />
                    </div>
                    
                </div>
                <button type="submit" class="btn btn-primary mt-4">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>