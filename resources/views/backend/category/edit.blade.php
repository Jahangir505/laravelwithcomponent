<x-admin.layout.master>
    <x-slot:title>Category </x-slot:title>
    <x-slot:pageTitle>Update </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header"> 
            <a href="/admin/categories" class="btn btn-info">Category List</a>
        </div>
        <form action="/admin/category/store" method="POST" >
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="name" class="form-label">Name:</label>
                            <input type="text" class="form-control" id="name" value="{{ @$category->name}}" placeholder="Enter Name" name="name">
                          </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="image" class="form-label">Image:</label>
                            <input type="file" class="form-control" id="image" value="{{ @$category->image}}" placeholder="Enter Image" name="image">
                          </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mb-3 mt-3">
                            <label for="description" class="form-label">Description:</label>
                            <textarea type="text" class="form-control" id="description" value="{{ @$category->description}}" placeholder="Enter description" name="description"></textarea>
                          </div>
                    </div>
                   
                    <input type="hidden" value="{{ @$category->id}}" name="id">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>