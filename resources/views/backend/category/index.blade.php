<x-admin.layout.master>
    @push('css')
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css"> 
    @endpush
    <x-slot:title>Category </x-slot:title>
    <x-slot:pageTitle>Category List </x-slot:pAddressTitle>
   
    
    <div class="card mb-4">
        <div class="card-header">
           
            <a href="/admin/category/create" class="btn btn-info">Add New</a>
        </div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Color</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Color Code</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($categories as $key=>$item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td><img src="{{ asset('ui/backend/assets/img') .'/' . @$item->image_uri ?? "default.jpg"}}" alt="{{ $item->name}}" width="30" /></td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->description}}</td>
                        <td>
                            {{ $item->color}}
                        </td>
                        <td>
                            <a href="/admin/brand/edit?id={{$item->id}}" class="btn btn-primary btn-sm">Edit</a>
                            <a href="" class="btn btn-info btn-sm">View</a>
                            <a href="/admin/brand/delete?id={{$item->id}}" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                    
                   
                </tbody>
            </table>
        </div>
    </div>

    
    
</x-admin.layout.master>