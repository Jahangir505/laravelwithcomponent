<x-admin.layout.master>
    <x-slot:title>Product </x-slot:title>
    <x-slot:pageTitle>Create Product</x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
           
            <a href="/admin/products" class="btn btn-info">Product List</a>
        </div>
        <form action="{{url('/admin/product/store')}}" method="POST" >
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="name" class="form-label">Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
                          </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="category" class="form-label">Select Category</label>
                            <select name="category_id" id="category" class="form-control">
                                <option value="">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{ @$category->id }}">{{ @$category->name}}</option>
                                @endforeach
                            </select>
                          </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="brand" class="form-label">Select Brand</label>
                            <select name="brand_id" id="brand" class="form-control">
                                <option value="">Select Brand</option>
                                @foreach($brands as $brand)
                                    <option value="{{ @$brand->id }}">{{ @$brand->name}}</option>
                                @endforeach
                            </select>
                          </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="brand" class="form-label">Select Unit</label>
                            <select name="brand_id" id="brand" class="form-control">
                                <option value="">Select Unit</option>
                                @foreach($units as $unit)
                                    <option value="{{ @$unit->id }}">{{ @$unit->name}}</option>
                                @endforeach
                            </select>
                          </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3 mt-3">
                            <label for="sale_price" class="form-label">Sales Price:</label>
                            <input type="text" class="form-control" id="sale_price" placeholder="Enter sale price" name="sale_price">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3 mt-3">
                            <label for="purchase_price" class="form-label">Purchase Price:</label>
                            <input type="text" class="form-control" id="purchase_price" placeholder="Enter purchase price" name="purchase_price">
                          </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3 mt-3">
                            <label for="min_stock" class="form-label">Min Stock:</label>
                            <input type="text" class="form-control" id="min_stock" placeholder="Enter min stock" name="min_stock">
                          </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>