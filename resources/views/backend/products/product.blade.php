<x-admin.layout.master>
    <x-slot:title>Product </x-slot:title>
    <x-slot:pageTitle>Product List </x-slot:pageTitle>

    <div class="card mb-4">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="card-header">
           
            <a href="/admin/create" class="btn btn-info">Add New</a>
        </div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Brand</th>
                        <th>Sale Price</th>
                        <th>Purchase Price</th>
                        <th>Min Stock</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Brand</th>
                        <th>Sale Price</th>
                        <th>Purchase Price</th>
                        <th>Min Stock</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($products as $data)
                        <tr>
                            <td>{{ @$data->title}}</td>
                            <td>{{ @$data->category->name}}</td>
                            <td>{{ @$data->brand->name}}</td>
                            <td>{{ @$data->sale_price}}</td>
                            <td>{{ @$data->purchase_price}}</td>
                            <td>{{ @$data->min_stock}}</td>
                            <td>
                                <a href="/admin/product/edit?id={{@$data->id}}" class="btn btn-primary btn-sm">Edit</a>
                                <a href="" class="btn btn-info btn-sm">View</a>
                                <a href="/admin/product/delete?id={{@$data->id}}" class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                    @endforeach
  
                </tbody>
            </table>
        </div>
    </div>

</x-admin.layout.master>