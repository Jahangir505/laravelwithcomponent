<x-admin.layout.master>
    <x-slot:title>Supplier </x-slot:title>
    <x-slot:pageTitle>Update </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
           
            <a href="/admin/suppliers" class="btn btn-info">Supplier List</a>
        </div>
        <form action="/admin/supplier/store" method="POST" >
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="name" class="form-label">Name:</label>
                            <input type="text" class="form-control" id="name" value="{{ @$supplier->name}}" placeholder="Enter Name" name="name">
                          </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="email" class="form-label">Email:</label>
                            <input type="email" class="form-control" id="email" value="{{ @$supplier->email}}" placeholder="Enter email" name="email">
                          </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="phone" class="form-label">Phone:</label>
                            <input type="text" class="form-control" id="phone" value="{{ @$supplier->phone}}" placeholder="Enter phone" name="phone">
                          </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <label for="address" class="form-label">Address:</label>
                            <input type="text" class="form-control" id="email" value="{{ @$supplier->address}}" placeholder="Enter address" name="address">
                          </div>
                    </div>
                    <input type="hidden" value="{{ @$supplier->id}}" name="id">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>