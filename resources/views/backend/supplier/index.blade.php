<x-admin.layout.master>
    <x-slot:title>Supplier </x-slot:title>
    <x-slot:pageTitle>Supplier List </x-slot:pAddressTitle>

    <div class="card mb-4">
        <div class="card-header">
           
            <a href="/admin/supplier/create" class="btn btn-info">Add New</a>
        </div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($suppliers as $key=>$item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email}}</td>
                        <td>{{ $item->phone}}</td>
                        <td>{{ $item->address}}</td>
                        <td>
                            <a href="/admin/supplier/edit?id={{$item->id}}" class="btn btn-primary btn-sm">Edit</a>
                            <a href="" class="btn btn-info btn-sm">View</a>
                            <a href="/admin/supplier/delete?id={{$item->id}}" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                    
                   
                </tbody>
            </table>
        </div>
    </div>

</x-admin.layout.master>