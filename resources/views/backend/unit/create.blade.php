<x-admin.layout.master>
    <x-slot:title>Unit </x-slot:title>
    <x-slot:pageTitle>Create Unit </x-slot:pageTitle>
    <div class="card p-4">
        {{-- <x-admin.utilities.errors/> --}}
        <div class="card-header">
            <a href="/admin/units" class="btn btn-info">Unit List</a>
        </div>
        <form action="/admin/unit/store" method="POST" >
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="name" placeholder="Enter your name" label="Name:" labelFor="name"  id="name" type="text" value="{{ old('name') }}" /> 
                        <x-admin.utilities.form.error name="name" />
                    </div>
                    
                </div>
                <button type="submit" class="btn btn-primary mt-4">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>