<x-admin.layout.master>
    <x-slot:title>Color </x-slot:title>
    <x-slot:pageTitle>Update Color </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
           
            <a href="{{ route('colors.index') }}" class="btn btn-info">Color List</a>
        </div>
        <form action="{{ route('colors.update', ['color' => $color->id]) }}" method="POST" >
            @csrf
            @method('patch')
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="title" placeholder="Enter your color name" label="Name:" labelFor="title"  id="title" type="text" value="{{ old('title', $color->title) }}" /> 
                        <x-admin.utilities.form.error name="title" />
                    </div>
                    
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="color" placeholder="Pic your color" label="Pick Color:" labelFor="color" class="form-control-color"  id="color" type="color" value="{{ old('color', $color->color_code) }}" /> 
                        {{-- <x-admin.utilities.form.error name="name" /> --}}
                    </div>

                </div>
                <button type="submit" class="btn btn-primary mt-4">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>