<x-admin.layout.master>
    @push('css')
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css"> 
    @endpush
    <x-slot:title>Color List</x-slot:title>
    <x-slot:pageTitle>Color List </x-slot:pAddressTitle>
   
    
    <div class="card mb-4">
        <div class="card-header">
           
            <a href="{{ route('colors.create')}}" class="btn btn-info">Add New</a>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Color Code</th>
                        <th>Color</th>
                        <th style="width: 20%">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Color Code</th>
                        <th>Color</th>
                        <th style="width: 20%">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($colors as $key=>$item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->color_code}}</td>
                        <td>
                            <div style="width:100%; height:30px; background-color: {{$item->color_code }}"></div>
                        </td>
                        <td>
                            <a href="{{ route('colors.edit', ['color' => $item->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                            <a href="" class="btn btn-info btn-sm">View</a>

                            <form action="{{ route('colors.destroy', ['color' => $item->id]) }}" method="POST" style="display: inline">
                                @csrf
                                @method('delete')
                                <button onclick="confirm('Are you sure want to delete?')" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                           
                        </td>
                    </tr>
                    @endforeach
                    
                  
                </tbody>
            </table>
            {{ $colors->links() }}
        </div>
    </div>

    
    
</x-admin.layout.master>