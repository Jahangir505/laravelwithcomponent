<x-admin.layout.master>
    <x-slot:title>Country </x-slot:title>
    <x-slot:pageTitle>Create Country </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
           
            <a href="{{ route('countries.index') }}" class="btn btn-info">Country List</a>
        </div>
        <form action="{{ route('countries.store') }}" method="POST" >
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="name" placeholder="Enter your country name" label="Name:" labelFor="name"  id="name" type="text" value="{{ old('name') }}" /> 
                        <x-admin.utilities.form.error name="name" />
                    </div>
                    
                   
                </div>
                <button type="submit" class="btn btn-primary mt-4">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>