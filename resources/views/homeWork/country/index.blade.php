<x-admin.layout.master>
    @push('css')
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css"> 
    @endpush
    <x-slot:title>Country List</x-slot:title>
    <x-slot:pageTitle>Country List </x-slot:pAddressTitle>
   
    
    <div class="card mb-4">
        <div class="card-header">
           
            <a href="{{ route('countries.create')}}" class="btn btn-info">Add New</a>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th style="width: 20%">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($countries as $key=>$item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->title }}</td>
                        <td>
                            <a href="{{ route('countries.edit', ['country' => $item->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                            <a href="" class="btn btn-info btn-sm">View</a>

                            <form action="{{ route('countries.destroy', ['country' => $item->id]) }}" method="POST" style="display: inline">
                                @csrf
                                @method('delete')
                                <button onclick="confirm('Are you sure want to delete?')" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                           
                        </td>
                    </tr>
                    @endforeach
                    
                   
                </tbody>
            </table>
        </div>
    </div>

    
    
</x-admin.layout.master>