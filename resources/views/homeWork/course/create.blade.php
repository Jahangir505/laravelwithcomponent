<x-admin.layout.master>
    <x-slot:title>Course </x-slot:title>
    <x-slot:pageTitle>Create Course </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
           
            <a href="{{ route('courses.index') }}" class="btn btn-info">Course List</a>
        </div>
        <form action="{{ route('courses.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="title" placeholder="Enter course name" label="Title:" labelFor="title"  id="title" type="text" value="{{ old('title') }}" /> 
                        <x-admin.utilities.form.error name="title" />
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="batch_no" placeholder="Example: Pondit-5" label="Batch No:" labelFor="batch_no"  id="batch_no" type="text" value="{{ old('batch_no') }}" /> 
                        <x-admin.utilities.form.error name="batch_no" />
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="start_date" placeholder="Example: Pondit-5" label="Start Date:" labelFor="start_date"  id="start_date" type="date" value="{{ old('start_date') }}" /> 
                        <x-admin.utilities.form.error name="start_date" />
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="end_date" placeholder="Example: Pondit-5" label="End Date:" labelFor="end_date"  id="end_date" type="date" value="{{ old('end_date') }}" /> 
                        <x-admin.utilities.form.error name="end_date" />
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="instructor_name" placeholder="Enter course name" label="Instructor Name:" labelFor="instructor_name"  id="instructor_name" type="text" value="{{ old('instructor_name') }}" /> 
                        <x-admin.utilities.form.error name="instructor_name" />
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="image" placeholder="Upload your image" label="Image:" labelFor="iamge"  id="image" type="file" value="{{ old('image') }}" /> 
                        {{-- <x-admin.utilities.form.error name="name" /> --}}
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3 mt-3">
                            <input type="checkbox"  id="is_active" placeholder="Enter address" value="1" name="is_active">
                            <label for="is_active" class="form-label">Is Active:</label>
                          </div>
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.select name="course_type"  label="Course Type:" labelFor="course_type"  id="course_type"  /> 
                        <x-admin.utilities.form.error name="course_type" />
                    </div>
                    
                </div>
                <button type="submit" class="btn btn-primary mt-4">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>