<x-admin.layout.master>
    @push('css')
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css"> 
    @endpush
    <x-slot:title>Course </x-slot:title>
    <x-slot:pageTitle>Course List </x-slot:pAddressTitle>
   
    
    <div class="card mb-4">
        <div class="card-header">
           
            <a href="{{ route('courses.create') }}" class="btn btn-info">Add New</a>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Instructor Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th style="width:180px; ">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Instructor Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th style="width:120px; ">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($courses as $key=>$item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td><img src="{{ asset('storage/courses') .'/' . @$item->banner_uri ?? "default.jpg"}}" alt="{{ $item->name}}" width="30" /></td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->instructor_name }}</td>
                        <td>{{ $item->start_date }}</td>
                        <td>{{ $item->end_date }}</td>
                        <td><span class="badge {{ $item->is_active ? 'bg-primary' : 'bg-secondary'}}">{{ $item->is_active ? 'Active' : 'In Active'}}</span></td>
                       
                        <td>
                            <a href="{{ route('courses.edit', ['course' => $item->id])}}" class="btn btn-primary btn-sm">Edit</a>
                            <a href="{{ route('courses.show', ['course' => $item->id]) }}" class="btn btn-info btn-sm">View</a>
                            <form action="{{ route('courses.destroy', ['course' => $item->id]) }}" method="POST" style="display: inline">
                                @csrf
                                @method('delete')
                                <button onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    
                   
                </tbody>
            </table>
            {{ $courses->links()}}
        </div>
    </div>

    
    
</x-admin.layout.master>