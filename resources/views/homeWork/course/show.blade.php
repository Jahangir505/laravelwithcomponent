<x-admin.layout.master>
    @push('css')
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css"> 
    @endpush
    <x-slot:title>Course Show</x-slot:title>
    <x-slot:pageTitle>Course Show </x-slot:pAddressTitle>
   
    
    <div class="card mb-4">
        <div class="card-header">
           
            <a href="{{ route('courses.index') }}" class="btn btn-info">Course List</a>
        </div>
        <div class="card-body">
            <table class="table">
                <tr>
                    <th>Title:</th>
                    <td>{{ $course->title}}</td>
                    <th>Mentor Name</th>
                    <td>{{ $course->instructor_name}}</td>
                    <th>Batch</th>
                    <td>{{ $course->batch_no}}</td>
                </tr>
                <tr>
                    <th>
                        Start Date
                    </th>
                    <td>{{ $course->start_date}}</td>
                    <th>
                        End Date
                    </th>
                    <td>{{ $course->end_date}}</td>
                    <th>
                        Status
                    </th>
                    <td>{{ $course->is_active ? 'Active' : 'In Active'}}</td>
                </tr>
                <tr>
                    <th>Course Type</th>
                    <td>{{ $course->course_type }}</td>
                    <th>Banner</th>
                    <td>
                        <img src="{{ asset('storage/courses') .'/' . @$course->banner_uri ?? "default.jpg"}}" alt="{{ $course->name}}" width="50" />
                    </td>
                </tr>
            </table>
        </div>
    </div>

    
    
</x-admin.layout.master>