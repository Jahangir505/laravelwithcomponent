<x-admin.layout.master>
    <x-slot:title>Tag </x-slot:title>
    <x-slot:pageTitle>Create Tag </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
           
            <a href="{{ route('tags.index') }}" class="btn btn-info">Tag List</a>
        </div>
        <form action="{{ route('tags.store') }}" method="POST" >
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="title" placeholder="Enter your color name" label="Name:" labelFor="title"  id="title" type="text" value="{{ old('title') }}" /> 
                        <x-admin.utilities.form.error name="title" />
                    </div>
                
                </div>
                <button type="submit" class="btn btn-primary mt-4">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layout.master>