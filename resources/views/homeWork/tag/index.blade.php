<x-admin.layout.master>
    @push('css')
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css"> 
    @endpush
    <x-slot:title>Tag List</x-slot:title>
    <x-slot:pageTitle>Tag List </x-slot:pAddressTitle>
   
    
    <div class="card mb-4">
        <div class="card-header">
           
            <a href="{{ route('tags.create')}}" class="btn btn-info">Add New</a>
            <a href="{{ route('tags.pdf')}}" class="btn btn-primary">Download PDF</a>
        </div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($tags as $key=>$item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->title }}</td>
                        <td>
                            <a href="{{ route('tags.edit', ['tag' => $item->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                            <a href="" class="btn btn-info btn-sm">View</a>

                            <form action="{{ route('tags.destroy', ['tag' => $item->id]) }}" method="POST" style="display: inline">
                                @csrf
                                @method('delete')
                                <button onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                           
                        </td>
                    </tr>
                    @endforeach
                    
                   
                </tbody>
            </table>
        </div>
    </div>

    
    
</x-admin.layout.master>