<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ErrorController;
use App\Http\Controllers\FrontEnd\FrontendController;
use App\Http\Controllers\HomeWork\ColorController;
use App\Http\Controllers\HomeWork\CountryController;
use App\Http\Controllers\HomeWork\TagController;
use App\Http\Controllers\Private\BrandController;
use App\Http\Controllers\Private\CategoryController;
use App\Http\Controllers\Private\ProductController;
use App\Http\Controllers\Private\SupplierController;
use App\Http\Controllers\Private\UnitController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

Route::get('/register', [DashboardController::class, 'registration']);
Route::get('/login', [DashboardController::class, 'login']);
Route::post('/register', [AuthController::class, 'register'])->name('auth.register');
Route::post('/login', [AuthController::class, 'login']);

Route::get('/403', [ErrorController::class, 'forbidden'])->name('error.403');

Route::middleware('admin')->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
    Route::prefix('admin')->group(function () {
        Route::get('/', [DashboardController::class, 'index']);

        // Product
        Route::get('/products', [ProductController::class, 'index']);
        Route::get('/product/create', [ProductController::class, 'create']);
        Route::post('/product/store', [ProductController::class, 'store']);

        // Supplier
        Route::get('/suppliers', [SupplierController::class, 'index']);
        Route::get('/supplier/create', [SupplierController::class, 'create']);
        Route::post('/supplier/store', [SupplierController::class, 'store']);
        Route::get('/supplier/edit', [SupplierController::class, 'edit']);
        Route::get('/supplier/delete', [SupplierController::class, 'delete']);

        // Category
        Route::get('/categories', [CategoryController::class, 'index']);
        Route::get('/category/create', [CategoryController::class, 'create']);
        Route::post('/category/store', [CategoryController::class, 'store']);
        Route::get('/category/edit', [CategoryController::class, 'edit']);
        Route::get('/category/delete', [CategoryController::class, 'delete']);

        // Brand
        Route::get('/brands', [BrandController::class, 'index']);
        Route::get('/brand/create', [BrandController::class, 'create']);
        Route::post('/brand/store', [BrandController::class, 'store']);
        Route::get('/brand/edit', [BrandController::class, 'edit']);
        Route::get('/brand/delete', [BrandController::class, 'delete']);

        // Brand
        Route::get('/units', [UnitController::class, 'index']);
        Route::get('/unit/create', [UnitController::class, 'create']);
        Route::post('/unit/store', [UnitController::class, 'store']);
        Route::get('/unit/edit', [UnitController::class, 'edit']);
        Route::get('/unit/delete', [UnitController::class, 'delete']);
    });
});




Route::prefix('frontend')->group(function () {
    Route::get('/', [FrontendController::class, 'index']);
    Route::get('/product-list', [FrontendController::class, 'productList']);
    Route::get('/product-details', [FrontendController::class, 'productDetails']);
    Route::get('/cart', [FrontendController::class, 'productCart']);
    Route::get('/checkout', [FrontendController::class, 'checkout']);
    Route::get('/order', [FrontendController::class, 'order']);
});

Route::prefix('homework')->group(function () {
    Route::get('/', [DashboardController::class, 'index']);
    Route::resource('colors', ColorController::class);
    Route::resource('countries', CountryController::class);
    Route::resource('tags', TagController::class);
    Route::get('/tag/download/pdf', [TagController::class, 'downloadPdf'])->name('tags.pdf');

    Route::resource('courses', CourseController::class);
});
